package org.example.fib;

import static org.junit.jupiter.api.Assertions.*;

import org.example.fib.MyMath;
import org.junit.jupiter.api.Test;

/**
 * Test some fibonacci number calculations
 */
class MyMathTest {

	/**
	 * Test the early numbers that don't invoke recursion.
	 */
	@Test
	void testFibTrivials() {
		for (int i = 0; i < 2; i++)
			assertEquals(i, MyMath.fib(i));
	}

	/**
	 * Fib(2)
	 */
	@Test
	void testFib3() {
		assertEquals(2, MyMath.fib(3));
	}

	/**
	 * Fib(5)
	 */
	@Test
	void testFib5() {
		assertEquals(5, MyMath.fib(5));
	}

	/**
	 * Fib(7)
	 */
	@Test
	void testFib7() {
		assertEquals(13, MyMath.fib(7));
	}

	/**
	 * Fib (9)
	 */
	@Test
	void testFib9() {
		assertEquals(34, MyMath.fib(9));
	}

	/**
	 * Fib (27)
	 */
	@Test
	void testFib27() {
		assertEquals(196418, MyMath.fib(27));
	}

	/**
	 * Test a bad argument
	 */
	@Test
	void testBadArg() {
		try {
			MyMath.fib(-1);
			fail("Expected an exception here.");
		} catch (Exception e) {

			// Make sure we're throwing the expected exception. (And not something else.)
			assertEquals("Non-negative integers only please.", e.getMessage());
		}
	}

	/**
	 * Instantiate the class, just to keep our code coverage reports happy. Method
	 * are static, so failing to initialize a class will make Jacoco complain.
	 */
	@Test
	void testCtor() {
		new MyMath();
	}

}
