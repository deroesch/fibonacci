package org.example.fib;

/**
 * Fun with recursion.
 */
public class MyMath {

	/**
	 * Computes the nth fibonacci number
	 * 
	 * @param n The desired fibonacci number
	 * @return The value of the desired fibonacci number
	 */
	public static int fib(int n) {

		if (n < 0)
			throw new IllegalArgumentException("Non-negative integers only please.");

		if (n < 2)
			return n;
		else
			return fib(n - 1) + fib(n - 2);
	}

}
